import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { portfolioApi } from "./portfolioApi";
import loginReducer from "../features/loginSlice";
import signupReducer from "../features/signupSlice";
import newStockReducer from "../features/portfolioSlice";

export const store = configureStore({
  reducer: {
    login: loginReducer,
    signup: signupReducer,
    newStock: newStockReducer,
    [portfolioApi.reducerPath]: portfolioApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([portfolioApi.middleware]),
});

setupListeners(store.dispatch);
