import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showToast: false,
};

const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    setShowToast(state, action) {
      state.showToast = action.payload;
    },
  },
});

export const { setShowToast } = formSlice.actions;
export default formSlice.reducer;
