import { useNavigate, Link } from "react-router-dom";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  handlePasswordChange,
  handleUsernameChange,
  reset,
} from "../../features/loginSlice";
import { useLoginMutation } from "../../app/portfolioApi";
import { useState } from "react";
import Alert from "react-bootstrap/Alert";
import Sound from "../../media/sounds/transition.mp3";
import errorSound from "../../media/sounds/ouch.mp3";

const LoginForm = () => {
  const [account, setAccount] = useState(false);
  const dispatch = useDispatch();
  const [login] = useLoginMutation();
  const { fields } = useSelector((state) => state.login);

  const navigate = useNavigate();

  const audio = new Audio(Sound);
  const errorAudio = new Audio(errorSound);

  const handleSubmit = (e) => {
    e.preventDefault();
    login(fields)
      .unwrap()
      .then((payload) => {
        if (payload) {
          navigate("/");
          audio.play();
        }
      })
      .catch((error) => {
        if (error) {
          errorAudio.play();
          setAccount(true);
          dispatch(reset());
        }
      });
  };

  return (
    <>
      <div className="container ">
        <div className="shadow p-4 col-3 position-absolute top-50 start-50 translate-middle bg-white rounded border border-2">
          {(() => {
            switch (account) {
              case true:
                return (
                  <>
                    <Alert
                      variant="danger"
                      onClose={() => setAccount(false)}
                      dismissible
                    >
                      <Alert.Heading>
                        Account not found with that username and/or
                        password.
                      </Alert.Heading>
                      <p>Please try again.</p>
                    </Alert>
                  </>
                );
              case false:
                return (
                  <>
                    <h5 className="card-title text-primary font-weight-bold">
                      LOGIN
                    </h5>
                    <hr />
                    <form onSubmit={handleSubmit}>
                      <div className="mb-3">
                        <label
                          htmlFor="Login__username"
                          className="form-label"
                        >
                          Username:
                        </label>
                        <input
                          className="form-control form-control-sm"
                          type={`text`}
                          id="Login__username"
                          value={fields.username}
                          onChange={(e) =>
                            dispatch(handleUsernameChange(e.target.value))
                          }
                        />
                      </div>
                      <div className="mb-3">
                        <label
                          htmlFor="Login__password"
                          className="form-label"
                        >
                          Password:
                        </label>
                        <input
                          className="form-control form-control-sm"
                          type={`password`}
                          id="Login__password"
                          value={fields.password}
                          onChange={(e) =>
                            dispatch(handlePasswordChange(e.target.value))
                          }
                        />
                      </div>
                      <div className="text-center">
                        <p>
                          Don't have an account?&nbsp;
                          <Link className="text-primary"
                            to={{
                              pathname: "/signup",
                            }}>Sign up!
                          </Link>
                          </p>
                      </div>
                      <div className="text-center">
                        <button
                          type="submit"
                          className="btn btn-primary text-white"
                        >
                          LOGIN
                        </button>
                      </div>
                    </form>
                  </>
                );
              default:
                return;
            }
          })()}
        </div>
      </div>
    </>
  );
};

export default LoginForm;
