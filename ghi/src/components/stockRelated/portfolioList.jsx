import { useState } from "react";
import {
  useGetPortfolioQuery,
  useGetNewsQuery,
  useGetCashQuery,
  useGetAccountQuery,
} from "../../app/portfolioApi";
import loadingGif from "../../media/gifs/newwizard.gif";
import Chart1 from "./graphs/PieChart";
import { Link, useLocation } from "react-router-dom";
import NewsForPortfolio from "./newsForPortfolio";

export default function PortfolioList() {
  const { data: stocks, isLoading } = useGetPortfolioQuery();
  const { isLoading: newsLoading } = useGetNewsQuery();
  const { data: cash, isLoading: cashLoading } = useGetCashQuery();
  const { data: isLoggedIn, isLoading: accountLoading } =
    useGetAccountQuery();
  const [showPortfolio, setShowPortfolio] = useState(true);
  const location = useLocation();

  if (location.state === null) {
    location.state = {
      stockNavError: false,
      noStocksAccessingSellForm: false,
    };
  }

  const hidePortfolio = () => {
    if (showPortfolio === true) {
      return setShowPortfolio(false);
    } else {
      return setShowPortfolio(true);
    }
  };

  if (isLoading || newsLoading || cashLoading || accountLoading) {
    return (
      <>
        <div className="container">
          <img
            className="p-40 col-30 position-absolute top-50 start-50 translate-middle img-responsive"
            src={loadingGif}
            alt="wait until the page loads"
          />
        </div>
      </>
    );
  } else {
    let currentStocksValue = stocks.stocks.reduce(
      (accumulator, object) => {
        return accumulator + object.total;
      },
      0
    );
    currentStocksValue = Math.round(currentStocksValue * 100) / 100;

    let currentCash = Math.round(cash.total * 100) / 100;

    let totalCurrentMoney =
      Math.round((currentStocksValue + cash.total) * 100) / 100;
    let startedWithCash = 10000;

    let GainOrLossInPercents =
      (100 * totalCurrentMoney) / startedWithCash - 100;
    let GainOrLossInDollars =
      (startedWithCash * GainOrLossInPercents) / 100;

    GainOrLossInPercents = Math.round(GainOrLossInPercents * 100) / 100;
    GainOrLossInDollars = Math.round(GainOrLossInDollars * 100) / 100;

    const colorCode = GainOrLossInPercents > 0 ? "primary" : "danger";

    const arrowUp = (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-arrow-bar-up"
        viewBox="0 0 16 16"
      >
        <path
          fillRule="evenodd"
          d="M8 10a.5.5 0 0 0 .5-.5V3.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 3.707V9.5a.5.5 0 0 0 .5.5zm-7 2.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5z"
        />
      </svg>
    );

    const arrowDown = (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="16"
        height="16"
        fill="currentColor"
        className="bi bi-arrow-bar-down"
        viewBox="0 0 16 16"
      >
        <path
          fillRule="evenodd"
          d="M1 3.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5zM8 6a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 .708-.708L7.5 12.293V6.5A.5.5 0 0 1 8 6z"
        />
      </svg>
    );

    const arrowDirection = GainOrLossInPercents > 0 ? arrowUp : arrowDown;

    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return (
      <>
        <div className="container-xl mt-2 mb-2 card shadow">
          <h2 className="text-primary font-weight-bold mt-2">
            <em className="text-capitalize">{isLoggedIn.username}</em>'s
            Portfolio Overview
          </h2>
          {location.state.stockNavError && (
            <div className="alert alert-danger text-center" role="alert">
              You must buy at least one stock to see your statistics! Buy
              one now!
            </div>
          )}
          {location.state.noStocksAccessingSellForm && (
            <div className="alert alert-danger text-center" role="alert">
              You can't sell shares if you don't have any! Buy one now!
            </div>
          )}
          {showPortfolio ? (
            <>
              <div>
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3"
                  onClick={hidePortfolio}
                >
                  Hide Portfolio
                </button>
              </div>
            </>
          ) : (
            <>
              <div>
                <button
                  type="button"
                  className="btn btn-primary text-white mb-3"
                  onClick={hidePortfolio}
                >
                  Show Portfolio
                </button>
              </div>
            </>
          )}
          {showPortfolio ? (
            <div className="row align-items-center">
              <div className="col">
                <table className="table table-striped">
                  <tbody>
                    <tr>
                      <th scope="row">Started practicing with:</th>
                      <td className="fw-bolder">$10,000</td>
                    </tr>
                    <tr>
                      <th scope="row">Investing:</th>
                      <td className="fw-bolder">
                        ${numberWithCommas(currentStocksValue)}
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">Buying power:</th>
                      <td className="fw-bolder">
                        ${numberWithCommas(currentCash)}
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">Total gain/loss by percentage:</th>
                      <td className={`text-${colorCode} fw-bolder`}>
                        {arrowDirection}{" "}
                        {numberWithCommas(Math.abs(GainOrLossInPercents))}%
                      </td>
                    </tr>
                    <tr>
                      <th scope="row">Total gain/loss:</th>
                      <td className={`text-${colorCode} fw-bolder`}>
                        {arrowDirection} $
                        {numberWithCommas(Math.abs(GainOrLossInDollars))}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col">
                <Chart1 stocks={stocks.stocks} />
              </div>
            </div>
          ) : (
            <div className="row align-items-center">
              <div className="col">
                <table className="table table-striped">
                  <tbody>
                    <tr>
                      <th>Started practicing with:</th>
                      <td>*******</td>
                    </tr>
                    <tr>
                      <th scope="row">Investing:</th>
                      <td>*******</td>
                    </tr>
                    <tr>
                      <th scope="row">Buying power:</th>
                      <td>*******</td>
                    </tr>
                    <tr>
                      <th scope="row">Total gain/loss by percentage:</th>
                      <td>*******</td>
                    </tr>
                    <tr>
                      <th scope="row">Total gain/loss:</th>
                      <td>*******</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div className="col">
                <Chart1 stocks={stocks.stocks} />
              </div>
            </div>
          )}
          <table className="table table-striped">
            <thead>
              {showPortfolio ? (
                <tr>
                  <th className="text-center">Name</th>
                  <th className="text-center">Symbol</th>
                  <th className="text-center">Shares</th>
                  <th className="text-center">Price</th>
                  <th className="text-center">Total</th>
                  <th className="text-center"></th>
                </tr>
              ) : (
                <tr></tr>
              )}
            </thead>
            <tbody className="bg-light">
              {stocks.stocks.length === 0 ? (
                <>
                  <tr className="bg-light align-middle">
                    <td colSpan="6" className="text-center">
                      You have no stocks! Buy your first stock with the buy
                      button up above!
                    </td>
                  </tr>
                </>
              ) : (
                showPortfolio &&
                stocks.stocks.map((stock) => {
                  return (
                    <tr className="bg-light align-middle" key={stock.id}>
                      <td className="text-center">{stock.name}</td>
                      <td className="text-center">{stock.symbol}</td>
                      <td className="text-center">
                        {Math.round(stock.shares * 100) / 100}
                      </td>
                      <td className="text-center">
                        ${numberWithCommas(stock.price)}
                      </td>
                      <td className="text-center">
                        ${numberWithCommas(stock.total)}
                      </td>
                      <td className="text-center">
                        <Link
                          className="btn btn-primary btn-lg px-4 gap-3 text-white"
                          to={{
                            pathname: "/stock",
                          }}
                          state={{
                            stockId: stock.id,
                            stockSymbol: stock.symbol,
                          }}
                        >
                          Go to stock
                        </Link>
                      </td>
                    </tr>
                  );
                })
              )}
            </tbody>
          </table>
          <NewsForPortfolio />
        </div>
        <div className="clear"></div>
      </>
    );
  }
}
