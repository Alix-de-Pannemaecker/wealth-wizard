import { useGetOnePortfolioQuery } from "../../app/portfolioApi";
import { useState, useEffect } from "react";
import { useLocation, Link } from "react-router-dom";

export default function NewsForStockPage() {
  const location = useLocation();
  const { data: stock, isLoading } = useGetOnePortfolioQuery(
    location.state.stockId
  );
  const [showNews, setShowNews] = useState(true);

  useEffect(() => {
    const data = window.localStorage.getItem("STOCK_PAGE_NEWS");
    if (data !== null) setShowNews(JSON.parse(data));
  }, []);

  useEffect(() => {
    window.localStorage.setItem(
      "STOCK_PAGE_NEWS",
      JSON.stringify(showNews)
    );
  }, [showNews]);

  if (isLoading) {
  } else {
    return (
      <div className="container">
        {showNews ? (
          <>
            <button
              type="button"
              className="btn btn-primary text-white mb-3"
              onClick={() => {
                setShowNews(false);
              }}
            >
              Hide Related News
            </button>
            <h2 className="text-primary font-weight-bold">Related News</h2>
          </>
        ) : (
          <button
            type="button"
            className="btn btn-primary text-white mb-3"
            onClick={() => {
              setShowNews(true);
            }}
          >
            Show Related News
          </button>
        )}
        {showNews && (
          <div className="row">
            <div className="col">
              {stock.news
                .filter((_, idx) => idx % 3 === 0)
                .map((one_news) => {
                  return (
                    <div
                      key={one_news.title}
                      className="card mb-3 shadow text-center"
                    >
                      <img
                        src={one_news.thumbnail.resolutions[0].url}
                        className="card-img-top"
                        alt="thumbnail"
                      />
                      <div className="card-body">
                        <h5>{one_news.title}</h5>
                        <p className="card-text text-center">
                          <Link
                            target={"_blank"}
                            to={one_news.link}
                            className="btn btn-primary btn-lg px-4 gap-3 text-white"
                          >
                            Learn More
                          </Link>
                        </p>
                      </div>
                      <div className="card-footer">
                        Publisher: {one_news.publisher}
                      </div>
                    </div>
                  );
                })}
            </div>
            <div className="col">
              {stock.news
                .filter((_, idx) => idx % 3 === 1)
                .map((one_news) => {
                  return (
                    <div
                      key={one_news.title}
                      className="card mb-3 shadow text-center"
                    >
                      <img
                        src={one_news.thumbnail.resolutions[0].url}
                        className="card-img-top"
                        alt="thumbnail"
                      />
                      <div className="card-body">
                        <h5>{one_news.title}</h5>
                        <p className="card-text">
                          <Link
                            target={"_blank"}
                            to={one_news.link}
                            className="btn btn-primary btn-lg px-4 gap-3 text-white"
                          >
                            Learn More
                          </Link>
                        </p>
                      </div>
                      <div className="card-footer">
                        Publisher: {one_news.publisher}
                      </div>
                    </div>
                  );
                })}
            </div>
            <div className="col">
              {stock.news
                .filter((_, idx) => idx % 3 === 2)
                .map((one_news) => {
                  return (
                    <div
                      key={one_news.title}
                      className="card mb-3 shadow text-center"
                    >
                      <img
                        src={one_news.thumbnail.resolutions[0].url}
                        className="card-img-top"
                        alt="thumbnail"
                      />
                      <div className="card-body">
                        <h5>{one_news.title}</h5>
                        <p className="card-text">
                          <Link
                            target={"_blank"}
                            to={one_news.link}
                            className="btn btn-primary btn-lg px-4 gap-3 text-white"
                          >
                            Learn More
                          </Link>
                        </p>
                      </div>
                      <div className="card-footer">
                        Publisher: {one_news.publisher}
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        )}
      </div>
    );
  }
}
