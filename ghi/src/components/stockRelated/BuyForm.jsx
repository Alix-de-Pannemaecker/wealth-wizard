import { useSelector, useDispatch } from "react-redux";
import {
  handleSymbolChange,
  handleSharesChange,
  errorChange,
  reset,
} from "../../features/portfolioSlice";
import {
  useCreateBuyMutation,
} from "../../app/portfolioApi";
import { useNavigate } from "react-router-dom";
import ErrorMessage from "../errorHandling/ErrorMessage";
import buySound from "../../media/sounds/buy_sound.mp3";
import errorSound from "../../media/sounds/ouch.mp3";
import { useEffect } from "react";

const BuyForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { errorMessage, fields } = useSelector((state) => state.newStock);
  const [createBuy] = useCreateBuyMutation();
  const audio = new Audio(buySound);
  const errorAudio = new Audio(errorSound);

  useEffect(() => {
    dispatch(reset())
  }, [dispatch])

  const handleSubmit = (e) => {
    e.preventDefault();
    if (fields.shares <= 0) {
      errorAudio.play();
      dispatch(errorChange("Shares must be positive numeric value"))
      return
    }
    createBuy({ symbol: fields.symbol, shares: fields.shares })
      .unwrap()
      .then((payload) => {
        dispatch(reset())
        navigate("/portfolio")
        audio.play()
      })
      .catch((error) => {
        dispatch(errorChange(error.data.detail));
        errorAudio.play();
      });
  };

  return (
    <>
      <div className="container">
        <div className="shadow p-4 col-3 position-absolute top-50 start-50 translate-middle bg-white rounded border border-2">
          <h5 className="card-title text-primary font-weight-bold">BUY</h5>
          <hr />
          <form onSubmit={handleSubmit}>
            {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
            <div className="mb-3">
              <label htmlFor="Login__username" className="form-label">
                Symbol:
              </label>
              <input
                className="form-control form-control-sm"
                type={`text`}
                id="Login__username"
                placeholder="example: NFLX"
                value={fields.symbol}
                onChange={(e) => dispatch(handleSymbolChange(e.target.value))}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="Login__password" className="form-label">
                Shares:
              </label>
              <input
                className="form-control form-control-sm"
                type={`number`}
                id="Login__password"
                value={fields.shares}
                onChange={(e) => dispatch(handleSharesChange(e.target.value))}
              />
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-primary text-white">
                BUY
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};
export default BuyForm;
