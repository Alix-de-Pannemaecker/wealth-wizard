import {
  useGetPerformanceQuery,
  useGetOnePortfolioQuery,
  useCreateBuyMutation,
  useCreateSellMutation,
} from "../../app/portfolioApi";
import Chart2 from "./graphs/LineChart";
import loadingGif from "../../media/gifs/newwizard.gif";
import { useLocation, useNavigate } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { handleSharesChange, reset, errorChange } from "../../features/portfolioSlice";
import NewsForStockPage from "./newsForStockPage";
import { useEffect } from "react";
import buySound from "../../media/sounds/buy_sound.mp3";
import sellSound from "../../media/sounds/sell_sound.mp3";
import errorSound from "../../media/sounds/ouch.mp3";
import ErrorMessage from "../errorHandling/ErrorMessage";

function StockPage() {
  const audioBuy = new Audio(buySound);
  const audioSell = new Audio(sellSound);
  const errorAudio = new Audio(errorSound);

  const [showBuyModal, setBuyShow] = useState(false);
  const handleBuyClose = () => setBuyShow(false);
  const handleBuyShow = () => setBuyShow(true) && dispatch(reset());

  const [showSellModal, setSellShow] = useState(false);
  const handleSellClose = () => setSellShow(false);
  const handleSellShow = () => setSellShow(true) && dispatch(reset());

  const dispatch = useDispatch();

  const [createBuy] = useCreateBuyMutation();
  const [createSell] = useCreateSellMutation();

  const { errorMessage, fields } = useSelector((state) => state.newStock);

  const navigate = useNavigate();
  const location = useLocation();
  const { data: stock } = useGetOnePortfolioQuery(location.state?.stockId, { skip: location.state?.stockId ? false : true })
  const { data: performance } = useGetPerformanceQuery({
    symbol: location.state?.stockSymbol,
    period: "1mo",
  }, { skip: location.state?.stockSymbol ? false : true })
    const { data: performanceDay } = useGetPerformanceQuery({
    symbol: location.state?.stockSymbol,
    period: "1d",
  }, { skip: location.state?.stockSymbol ? false : true })

  const [showMonth, setShowMonth] = useState(true);
  const hideMonth = () => {
    if (showMonth === true) {
      return setShowMonth(false);
    } else {
      return setShowMonth(true);
    }
  };

  useEffect(() => {
    if (!location.state?.stockId) {
      navigate("/404");
      return;
    }
  })

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const handleBuySubmit = (e) => {
    e.preventDefault();

    if (fields.shares <= 0) {
      errorAudio.play();
      dispatch(errorChange("Shares must be positive numeric value"))
      return
    }

    createBuy({ symbol: stock.symbol, shares: fields.shares })
      .unwrap()
      .then((payload) => {
        dispatch(reset())
        setBuyShow(false)
        audioBuy.play()
      })
      .catch((error) => {
        dispatch(errorChange(error.data.detail));
        errorAudio.play();
      });
  };

  const sellAllStock = (e) => {
    e.preventDefault();
    audioSell.play();
    createSell({ symbol: stock.symbol, shares: stock.shares });
    dispatch(reset());
    navigate('/portfolio')
    return;
  };

  const handleSellSubmit = (e) => {
    e.preventDefault();
    if (fields.shares <= 0) {
      errorAudio.play();
      dispatch(errorChange("Shares must be positive numeric value"))
      return
    }
    if (Number(fields.shares) === stock.shares) {
      audioSell.play();
      createSell({ symbol: stock.symbol, shares: stock.shares });
      dispatch(reset());
      navigate('/portfolio')
      return;
    }
    createSell({ symbol: stock.symbol, shares: fields.shares })
      .unwrap()
      .then((payload) => {
        dispatch(reset())
        setSellShow(false)
        audioSell.play()
      })
      .catch((error) => {
        dispatch(errorChange(error.data.detail));
        errorAudio.play();
      });
  };

  const arrowUp = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-arrow-bar-up"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M8 10a.5.5 0 0 0 .5-.5V3.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 3.707V9.5a.5.5 0 0 0 .5.5zm-7 2.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5z"
      />
    </svg>
  );

  const arrowDown = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-arrow-bar-down"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M1 3.5a.5.5 0 0 1 .5-.5h13a.5.5 0 0 1 0 1h-13a.5.5 0 0 1-.5-.5zM8 6a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 .708-.708L7.5 12.293V6.5A.5.5 0 0 1 8 6z"
      />
    </svg>
  );

  if (!stock || !performance) {
    return (
      <>
        <div className="container">
          <img
            className="p-40 col-30 position-absolute top-50 start-50 translate-middle img-responsive"
            src={loadingGif}
            alt="wait until the page loads"
          />
        </div>
      </>
    );
  } else {

    let perf = performance.performance;
    let GainOrLossInPercents = Math.round((((100 * perf[perf.length - 1].close) / perf[0].close) -100 ) * 100) / 100;
    const arrowDirection = GainOrLossInPercents > 0 ? arrowUp : arrowDown;
    const colorCode = GainOrLossInPercents > 0 ? "primary" : "danger";

    let min = Number.POSITIVE_INFINITY;
    let max = Number.NEGATIVE_INFINITY;
    let tmp;
    for (let i = performance.performance.length - 1; i >= 0; i--) {
      tmp = perf[i].close;
      if (tmp < min) min = tmp;
      if (tmp > max) max = tmp;
    }
    min = Math.round(min * 100) / 100;
    max = Math.round(max * 100) / 100;

    let perfDay = performanceDay.performance;
    let GainOrLossInPercentsDay = Math.round((((100 * perfDay[perfDay.length - 1].close) / perfDay[0].close) -100 ) * 100) / 100;
    const arrowDirectionDay = GainOrLossInPercentsDay > 0 ? arrowUp : arrowDown;
    const colorCodeDay = GainOrLossInPercentsDay > 0 ? "primary" : "danger";

    let minDay = Number.POSITIVE_INFINITY;
    let maxDay = Number.NEGATIVE_INFINITY;
    let tmpDay;
    for (let i = performanceDay.performance.length - 1; i >= 0; i--) {
      tmpDay = perfDay[i].close;
      if (tmpDay < minDay) minDay = tmpDay;
      if (tmpDay > maxDay) maxDay = tmpDay;
    }
    minDay = Math.round(minDay * 100) / 100;
    maxDay = Math.round(maxDay * 100) / 100;

    return (
      <>
        <div className="container-xl mt-2 mb-2 card shadow">
          <h2 className="text-primary font-weight-bold pt-2">
            {stock.name} - {stock.symbol}
          </h2>
          <div className="row">
            <div className="col">
              <div className="mb-3">
                {showMonth ? (
                  <>
                    <div>
                      <button
                        type="button"
                        className="btn btn-primary text-white mb-3 mt-3"
                        onClick={hideMonth}
                      >
                        Show Daily Trend
                      </button>
                      <h2 className="text-center pt-3">
                        Stock price over the month:
                        &nbsp;&nbsp;
                        <em className={`text-${colorCode} fw-bolder`}>
                          {arrowDirection}
                          {numberWithCommas(Math.abs(GainOrLossInPercents))}%
                        </em>
                      </h2>
                      <h4 className="text-center fst-italic">
                        (min: ${numberWithCommas(Math.abs(min))} - max: ${numberWithCommas(Math.abs(max))})
                      </h4>
                      <Chart2 performance={performance.performance} period="Month" />
                    </div>
                  </>
                ) : (
                  <>
                    <div>
                      <button
                        type="button"
                        className="btn btn-primary text-white mb-3 mt-3"
                        onClick={hideMonth}
                      >
                        Show Monthly Trend
                      </button>
                        <h2 className="text-center pt-3">
                          Stock price during the day:
                          &nbsp;&nbsp;
                          <em className={`text-${colorCodeDay} fw-bolder`}>
                            {arrowDirectionDay}
                            {numberWithCommas(Math.abs(GainOrLossInPercentsDay))}%
                          </em>
                        </h2>
                        <h4 className="text-center fst-italic">
                          (min: ${numberWithCommas(Math.abs(minDay))} - max: ${numberWithCommas(Math.abs(maxDay))})
                        </h4>
                        <Chart2 performance={performanceDay.performance} period="Day" />
                        <p className="text-center fs-5">
                          Note: The graph shows eastern time as it corresponds to the New York Stock Exchange.
                        </p>
                    </div>
                  </>
                )}
                <div className="text-center">
                  <h5>
                    Owned Shares: {stock.shares}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-coin mx-3 my-auto "
                      viewBox="0 0 16 16"
                    >
                      <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z" />
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                      <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z" />
                    </svg>
                    Current stock price: ${numberWithCommas(stock.price)}
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="32"
                      height="32"
                      fill="currentColor"
                      className="bi bi-coin mx-3 my-auto "
                      viewBox="0 0 16 16"
                    >
                      <path d="M5.5 9.511c.076.954.83 1.697 2.182 1.785V12h.6v-.709c1.4-.098 2.218-.846 2.218-1.932 0-.987-.626-1.496-1.745-1.76l-.473-.112V5.57c.6.068.982.396 1.074.85h1.052c-.076-.919-.864-1.638-2.126-1.716V4h-.6v.719c-1.195.117-2.01.836-2.01 1.853 0 .9.606 1.472 1.613 1.707l.397.098v2.034c-.615-.093-1.022-.43-1.114-.9H5.5zm2.177-2.166c-.59-.137-.91-.416-.91-.836 0-.47.345-.822.915-.925v1.76h-.005zm.692 1.193c.717.166 1.048.435 1.048.91 0 .542-.412.914-1.135.982V8.518l.087.02z" />
                      <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                      <path d="M8 13.5a5.5 5.5 0 1 1 0-11 5.5 5.5 0 0 1 0 11zm0 .5A6 6 0 1 0 8 2a6 6 0 0 0 0 12z" />
                    </svg>
                    Total Owned Value: ${numberWithCommas(stock.total)}
                  </h5>
                  <Button
                    className="btn btn-primary btn-lg px-4 gap-3 mx-3 mt-2 text-white"
                    variant="primary"
                    onClick={handleBuyShow}
                  >
                    BUY
                  </Button>
                  <Modal
                    centered
                    show={showBuyModal}
                    onHide={handleBuyClose}
                  >
                    <Modal.Header closeButton>
                      <Modal.Title className="text-primary">
                        BUY {stock.symbol}
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <form onSubmit={handleBuySubmit}>
                        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                        <div className="mb-3">
                          <label
                            htmlFor="Login__password"
                            className="form-label"
                          >
                            Shares:
                          </label>
                          <input
                            className="form-control form-control-sm"
                            type={`number`}
                            id="Login__password"
                            value={fields.shares}
                            onChange={(e) =>
                              dispatch(handleSharesChange(e.target.value))
                            }
                          />
                        </div>
                        <div className="text-center">
                          <button
                            type="submit"
                            className="btn btn-primary text-white"
                          >
                            BUY
                          </button>
                        </div>
                      </form>
                    </Modal.Body>
                  </Modal>
                  <Button
                    className="btn btn-primary btn-lg px-4 gap-3 mx-3 mt-2 text-white"
                    variant="primary"
                    onClick={handleSellShow}
                  >
                    SELL
                  </Button>
                  <Modal
                    centered
                    show={showSellModal}
                    onHide={handleSellClose}
                  >
                    <Modal.Header closeButton>
                      <Modal.Title className="text-primary">
                        SELL {stock.symbol}
                      </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <form onSubmit={handleSellSubmit}>
                        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
                        <div className="mb-3">
                          <label
                            htmlFor="Login__password"
                            className="form-label"
                          >
                            Shares:
                          </label>
                          <input
                            className="form-control form-control-sm"
                            type={`number`}
                            id="Login__password"
                            value={fields.shares}
                            onChange={(e) =>
                              dispatch(handleSharesChange(e.target.value))
                            }
                          />
                        </div>
                        <div className="text-center">
                          <button type="submit" className="btn btn-primary text-white mx-2">
                            SELL
                          </button>
                          <button type="button" className="btn btn-outline-primary mx-2" onClick={sellAllStock}>Sell All {stock.symbol}</button>
                        </div>
                      </form>
                    </Modal.Body>
                  </Modal>
                </div>
              </div>
            </div>
          </div>
          <NewsForStockPage />
        </div>
        <div className="clear"></div>
      </>
    );
  }
}

export default StockPage;
