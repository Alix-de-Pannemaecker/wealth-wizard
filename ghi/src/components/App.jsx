import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import NavBar from "./NavBar";
import Footer from "./Footer";
import LoginForm from "./authentication/LoginForm";
import PortfolioList from "./stockRelated/portfolioList";
import StockPage from "./stockRelated/stockPage"
import OrderHistory from "./stockRelated/orderHistory";
import SignupForm from "./authentication/SignupForm";
import Stats from "./stockRelated/statistics";
import ProtectedRoute from "./authentication/ProtectedRoute";
import Error404 from "./errorHandling/Error404";
import { useGetAccountQuery } from "../app/portfolioApi";
import NewBuyForm from "./stockRelated/BuyForm";
import NewSellForm from "./stockRelated/SellForm";

function App() {
  const { data: isLoggedIn, isLoading } = useGetAccountQuery();
  if (isLoading) {
  } else {
    return (
      <>
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route path="/" element={<MainPage />} />
            {!isLoggedIn && (
              <Route path="/login" element={<LoginForm />} />
            )}
            <Route path="/signup" element={<SignupForm />} />
            <Route
              path="/portfolio"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <PortfolioList />
                </ProtectedRoute>
              }
            />
            <Route
              path="/buy"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <NewBuyForm />
                </ProtectedRoute>
              }
            />
            <Route
              path="/sell"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <NewSellForm />
                </ProtectedRoute>
              }
            />
            <Route
              path="/stock"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <StockPage />
                </ProtectedRoute>
              }
            />
            <Route
              path="/history"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <OrderHistory />
                </ProtectedRoute>
              }
            />
            <Route
              path="/stats"
              element={
                <ProtectedRoute isLoggedIn={isLoggedIn}>
                  <Stats />
                </ProtectedRoute>
              }
            />
            <Route path="/404" element={<Error404 />} />
            <Route path="/*" element={<Error404 />} />
          </Routes>
          <Footer />
        </BrowserRouter>
      </>
    );
  }
}

export default App;
