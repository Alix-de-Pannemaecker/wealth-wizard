import { NavLink } from "react-router-dom";
import logo from "../media/pictures/logo.png";
import Navbar from "react-bootstrap/Navbar";
import Logout from "./authentication/logout";
import { useGetAccountQuery } from "../app/portfolioApi";
import loadingGif from "../media/gifs/newwizard.gif";

function NavBar() {
  const { data: isLoggedIn, isLoading } = useGetAccountQuery();
  if (isLoading) {
    return (
      <>
        <div className="container">
          <img
            className="position-absolute translate-middle img-responsive"
            src={loadingGif}
            alt="wait until the page loads"
          />
        </div>
      </>
    );
  } else {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-info pt-0 pb-0 border-white border-bottom border-3">
        <div className="container-fluid">
          <NavLink
            className="nav-link text-white pages ghost px-4 fw-bold"
            to="/"
          >
            Wealth Wizard
          </NavLink>
          <Navbar.Brand href="/">
            <img src={logo} width="75" height="68" alt="logo" />
          </Navbar.Brand>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="collapse navbar-collapse border-white border-start border-3"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white pages ghost px-4 fw-bold mt-4 mb-4"
                    to="/portfolio"
                  >
                    Portfolio
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white pages ghost px-4 fw-bold mt-4 mb-4"
                    to="/history"
                  >
                    History
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white pages ghost px-4 fw-bold mt-4 mb-4"
                    to="/stats"
                  >
                    Statistics
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white pages ghost px-4 fw-bold mt-4 mb-4"
                    to="/buy"
                  >
                    Buy
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white pages ghost px-4 fw-bold mt-4 mb-4"
                    to="/sell"
                  >
                    Sell
                  </NavLink>
                </li>
              )}
            </ul>
            <ul className="navbar-nav ms-auto">
              {!isLoggedIn && (
                <li className="nav-item ">
                  <NavLink
                    className="nav-link text-white newbutton px-4 fw-bold mt-4 mb-4"
                    to="/login"
                  >
                    Login
                  </NavLink>
                </li>
              )}
              {!isLoggedIn && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link text-white newbutton ghost mx-4 px-4 fw-bold mt-4 mb-4"
                    to="/signup"
                  >
                    Sign up
                  </NavLink>
                </li>
              )}
              {isLoggedIn && (
                <li>
                  <NavLink className="nav-link text-white" to="/">
                    <Logout />
                  </NavLink>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default NavBar;
