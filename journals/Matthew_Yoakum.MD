Monday-Thursday, March 20-23:
I forgot to do a journal entry for the first few days, and since everything we've done has all belended together, I'll just put them all in this single entry.

We had a very smooth first week as a group. Overall we had about 3-4 ideas for a project, but were able to decide on one that everyone agreed with very quickly. We went with a stock-tracking app because it seemed quite simple to implement, but if we had extra time there is tons we can add to it as stretch goals.

We made a nice detailed wireframe quite quickly. Overall the week has gone very smoothly.


Friday, March 24:

Today we worked more on our API paths. For this Avi was sharing his screen, and the rest of us talked it through with him as he wrote down our idea for the APIs.





Thursday, March 30:
Pretty chill day. We spent more time just talking and getting to know each other which was fun. We put the finishing touches on our backend and setup our navbar and footer for our front-end


Monday, April 3:
We started working on Redux as a group. We still haven't gotten it completely up and running, but we are making progress. Looking forward to learning about front-end authentication tomorrow!

Tuesday, April 4:
-started to setup redux

Wednesday, April 5:
-added list of stocks and create stock form

Thursday, April 6:
-added front-end authentication using redux

Friday, April 7:
- added news, pie chart and restyled paged on "portfolio" main page
- added custom background to pages
- linked portfolio page to stock page by assing in arguments to dynamically display data on stock page
- updated navbar
- updated graphs styles

Monday, April 17:
- created login form:
    - error handling
    - alert window pop up
    - if success, redirect to home page
- work in progress on seperate branch for now to implement the stats page:
    - added enpoint in back to get "performances" for all stocks
    - added nested hooks to get "performances" for all stocks
    - in pause for now: data available on "stats" page, need to work on the maths now and think about the graph

Tuesday, April 18:
- created sign up form
- created logout
- created sell form
- fixed long time errors with "doctype error json not recognized": had to remove the "galvannize auth" import and tags from App.js

Wednesday, April 19:
-touched up main page styling
-added buy/sell/signup forms
-fixed some backend logic so that our history page worked correctly when selling all of a stock
-alix added to the performance graph page
-fixed some invalidateTags spots so that our page would get new information without having to do a hard refresh

Thursday, April 20:
-Changed logo and footer
-got rid of console error on buy form

Monday, April 24:
-Began restyling mainpage

Tuesday, April 25:
-Finished restyling of mainpage. Added carousel, changed background image, changed colors, etc.

Wednesday, April 26:
-Added unit tests for create and get all stocks.
-Added unit tests for create and get all history.

Thursday, April 27:
-Did the README.MD
