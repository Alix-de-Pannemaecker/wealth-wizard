from queries.client import Queries
from models import Stock, StockParams, StockWithNews, News, Cash
from bson.objectid import ObjectId
from queries.finance_third_party import pricing_info, news_info
from fastapi import HTTPException
from datetime import datetime


class StocksQueries(Queries):
    COLLECTION = "stocks"
    HISTORY_COLLECTION = "history"

    def create(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        price_info = pricing_info(stock["symbol"], round(stock["shares"]))
        stock["name"] = price_info["name"]
        self.collection.insert_one(stock)
        stock.update(price_info)
        stock["id"] = str(stock["_id"])
        return Stock(**stock)

    def get_all(self, user_id: str) -> list[Stock]:
        stocks = []
        for stock in self.collection.find({"user_id": user_id}):
            if stock["name"] == "money":
                continue
            stock.update(pricing_info(stock["symbol"], stock["shares"]))
            stock["id"] = str(stock["_id"])
            stocks.append(Stock(**stock))
        return stocks

    def get_one(self, id: str, user_id: str) -> StockWithNews:
        try:
            filter = {"_id": ObjectId(id), "user_id": user_id}
        except:
            raise HTTPException(
                status_code=400,
                detail="Invalid, stock id must be a 12-byte input or a 24-character hex string",
            )
        result = self.collection.find_one(filter)
        if not result:
            raise HTTPException(status_code=404, detail="Stock not found")
        result.update(pricing_info(result["symbol"], result["shares"]))
        result["news"] = news_info(result["symbol"])
        result["id"] = str(result["_id"])
        return result

    def delete(self, id: str, user_id: str) -> bool:
        try:
            result = self.collection.delete_one(
                {"_id": ObjectId(id), "user_id": user_id}
            )
        except:
            raise HTTPException(
                status_code=404, detail="stock with that id does not exist"
            )
        return result.deleted_count == 1

    def update(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        try:
            filter = {"_id": ObjectId(id), "user_id": user_id}
        except:
            raise HTTPException(
                status_code=400, detail="stock with that id does not exist"
            )

        new_values = {"$set": {"shares": stock["shares"]}}
        self.collection.update_one(filter, new_values)
        return self.get_one(stock["id"], user_id)

    def get_all_news(self, user_id: str) -> list[News]:
        news_out = []
        for stock in self.collection.find({"user_id": user_id}):
            if stock["name"] == "money":
                continue
            news = {}
            news_stuff = news_info(stock["symbol"])
            news["thumbnail"] = news_stuff[0]["thumbnail"]["resolutions"][0][
                "url"
            ]
            news["publisher"] = news_stuff[0]["publisher"]
            news["link"] = news_stuff[0]["link"]
            news["title"] = news_stuff[0]["title"]
            news["type"] = news_stuff[0]["type"]
            if news not in news_out:
                news_out.append(news)
        return news_out

    def create_money(self, params: Cash, user_id: str) -> Cash:
        if list(self.collection.find({"symbol": "money", "user_id": user_id})):
            raise HTTPException(
                status_code=404,
                detail="""Nice try bud! lol look at you, I can't believe you actually thought that would work! I know you thought you were smart sneaking around
                                like a little rat thinking you could just figure out the route to my backend api but you're not smart, you just wasted your time you little grub.""",
            )
        else:
            cash = params.dict()
            cash["user_id"] = user_id
            cash["symbol"] = "money"
            cash["shares"] = cash["total"]
            cash["name"] = "money"
            self.collection.insert_one(cash)
            return Cash(**cash)

    def update_money(self, params: Cash, user_id: str) -> Cash:
        cash = params.dict()
        try:
            filter = {"symbol": "money", "user_id": user_id}
        except:
            raise HTTPException(
                status_code=404, detail="that user does not exist"
            )
        new_values = {"$set": {"total": cash["total"]}}
        self.collection.update_one(filter, new_values)
        return self.get_money(user_id)

    def get_money(self, user_id: str) -> Cash:
        filter = {"symbol": "money", "user_id": user_id}
        result = self.collection.find_one(filter)
        return result

    def delete_money(self, user_id: str) -> bool:
        result = self.collection.delete_one(
            {"symbol": "money", "user_id": user_id}
        )
        return result.deleted_count == 1

    def buy(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        if not isinstance(stock["shares"], (float, int)):
            raise HTTPException(status_code=400, detail="must contain shares")

        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        price_info = pricing_info(stock["symbol"], stock["shares"])
        cash_filter = {"symbol": "money", "user_id": user_id}
        result = self.collection.find_one(cash_filter)
        # Check if the stock is already in the collection
        # If so then update that stock
        for user_stock in self.collection.find({"user_id": user_id}):
            # if the stock exists in the user's portfolio check if they can afford it
            # if they can afford it
            # update that entry in the portfolio
            # update how much cash that user has
            # Create a new entry in history
            # else if they can't afford it return an error message
            if stock["symbol"] == user_stock["symbol"]:
                user_stock["id"] = str(user_stock["_id"])
                if result["total"] >= (price_info["total"]):
                    updated_total = result["total"] - (price_info["total"])
                    new_values = {"$set": {"total": updated_total}}
                    self.collection.update_one(cash_filter, new_values)

                    stock_filter = {
                        "_id": ObjectId(user_stock["id"]),
                        "user_id": user_id,
                    }
                    new_stock_values = {
                        "$set": {
                            "shares": (user_stock["shares"] + stock["shares"])
                        }
                    }
                    self.collection.update_one(stock_filter, new_stock_values)

                    d = datetime.now()
                    string = d.strftime("%m/%d/%Y, %I:%M%p")
                    new = d.strptime(string, "%m/%d/%Y, %I:%M%p")

                    history = {
                        "symbol": stock["symbol"],
                        "user_id": user_id,
                        "date": new,
                        "shares": stock["shares"],
                        "transaction_type": "BUY",
                        "id": user_stock["id"],
                    }
                    history.update(price_info)
                    self.history_collection.insert_one(history)

                    return self.get_one(user_stock["id"], user_id)
                else:
                    raise HTTPException(
                        status_code=402,
                        detail="Not enough funds to purchase this stock",
                    )

        if result["total"] >= (price_info["total"]):
            stock["name"] = price_info["name"]
            self.collection.insert_one(stock)
            stock.update(price_info)
            stock["id"] = str(stock["_id"])

            updated_total = result["total"] - (price_info["total"])
            new_values = {"$set": {"total": updated_total}}
            self.collection.update_one(cash_filter, new_values)

            d = datetime.now()
            string = d.strftime("%m/%d/%Y, %I:%M%p")
            new = d.strptime(string, "%m/%d/%Y, %I:%M%p")

            history = {
                "symbol": stock["symbol"],
                "user_id": user_id,
                "date": new,
                "shares": stock["shares"],
                "transaction_type": "BUY",
                "id": stock["id"],
            }
            history.update(price_info)
            self.history_collection.insert_one(history)

            return stock
        else:
            raise HTTPException(
                status_code=402,
                detail="Not enough funds to purchase this stock",
            )

    def sell(self, params: StockParams, user_id: str) -> Stock:
        stock = params.dict()
        stock["symbol"] = stock["symbol"].upper()
        stock["user_id"] = user_id
        price_info = pricing_info(stock["symbol"], stock["shares"])
        cash_filter = {"symbol": "money", "user_id": user_id}
        result = self.collection.find_one(cash_filter)
        # Check if the stock is already in the collection
        # If so then update that stock
        for user_stock in self.collection.find({"user_id": user_id}):
            if stock["symbol"] == user_stock["symbol"]:
                user_stock["id"] = str(user_stock["_id"])
                if user_stock["shares"] > (stock["shares"]):
                    updated_total = result["total"] + (price_info["total"])
                    new_values = {"$set": {"total": updated_total}}
                    self.collection.update_one(cash_filter, new_values)

                    stock_filter = {
                        "_id": ObjectId(user_stock["id"]),
                        "user_id": user_id,
                    }
                    new_stock_values = {
                        "$set": {
                            "shares": (user_stock["shares"] - stock["shares"])
                        }
                    }
                    self.collection.update_one(stock_filter, new_stock_values)

                    d = datetime.now()
                    string = d.strftime("%m/%d/%Y, %I:%M%p")
                    new = d.strptime(string, "%m/%d/%Y, %I:%M%p")

                    history = {
                        "symbol": stock["symbol"],
                        "user_id": user_id,
                        "date": new,
                        "shares": stock["shares"],
                        "transaction_type": "SELL",
                        "id": user_stock["id"],
                    }
                    history.update(price_info)
                    self.history_collection.insert_one(history)

                    return self.get_one(user_stock["id"], user_id)

                elif user_stock["shares"] == stock["shares"]:
                    try:
                        self.collection.delete_one(
                            {
                                "_id": ObjectId(user_stock["id"]),
                                "user_id": user_id,
                            }
                        )
                    except:
                        raise HTTPException(
                            status_code=404,
                            detail="stock with that id does not exist",
                        )

                    updated_total = result["total"] + (price_info["total"])
                    new_values = {"$set": {"total": updated_total}}
                    self.collection.update_one(cash_filter, new_values)

                    d = datetime.now()
                    string = d.strftime("%m/%d/%Y, %I:%M%p")
                    new = d.strptime(string, "%m/%d/%Y, %I:%M%p")

                    history = {
                        "symbol": stock["symbol"],
                        "user_id": user_id,
                        "date": new,
                        "shares": stock["shares"],
                        "transaction_type": "SELL",
                        "id": user_stock["id"],
                    }
                    history.update(price_info)
                    self.history_collection.insert_one(history)
                    raise HTTPException(
                        status_code=200, detail="stock removed from portfolio"
                    )

                else:
                    raise HTTPException(
                        status_code=400,
                        detail="You cannot sell more shares than you currently own",
                    )

        raise HTTPException(
            status_code=404,
            detail="You cannot sell a stock that you don't currently own",
        )
