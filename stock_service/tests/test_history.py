from fastapi.testclient import TestClient
from main import app
from queries.history import HistoryQueries
from authenticator import authenticator
from models import HistoryParams, History
from datetime import datetime


client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "fakeuser"}


class FakeHistoryQueries:
    def create(self, params: HistoryParams, user_id="fakeuser") -> History:
        history = params.dict()
        history["symbol"] = history["symbol"].upper()
        history["date"] = datetime.strptime(
            "04/26/2023, 12:13PM", "%m/%d/%Y, %I:%M%p"
        )
        history["user_id"] = user_id
        history["name"] = "AMC Entertainment Holdings, Inc."
        price = 9.00
        history.update({"price": 9.00, "total": price * history["shares"]})
        history["id"] = "64482332fae37cb0433e046c"

        return History(**history)

    def get_all(self, user_id: str) -> list[History]:
        history = {
            "symbol": "AMC",
            "shares": 3,
            "name": "AMC Entertainment Holdings, Inc.",
            "price": 9.00,
            "total": 27.00,
            "transaction_type": "BUY",
            "date": datetime.strptime(
                "04/26/2023, 12:13PM", "%m/%d/%Y, %I:%M%p"
            ),
        }
        historys = []
        history["user_id"] = user_id
        history["id"] = "64482332fae37cb0433e046c"
        historys.append(History(**history))

        return historys


def test_create_history():
    # Arrange
    app.dependency_overrides[HistoryQueries] = FakeHistoryQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    history = {"symbol": "AMC", "shares": 3, "transaction_type": "BUY"}

    # Act
    res = client.post("/api/history", json=history)
    data = res.json()

    # Assert
    assert data["total"] == 27.00
    assert data["symbol"] == "AMC"
    assert data["shares"] == 3
    assert data["price"] == 9.00
    assert data["name"] == "AMC Entertainment Holdings, Inc."
    assert data["id"] == "64482332fae37cb0433e046c"
    assert data["user_id"] == "fakeuser"
    assert (
        data["date"]
        == datetime.strptime(
            "04/26/2023, 12:13PM", "%m/%d/%Y, %I:%M%p"
        ).isoformat()
    )

    # Cleanup
    app.dependency_overrides = {}


def test_get_all_historys():
    # Arrange
    app.dependency_overrides[HistoryQueries] = FakeHistoryQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    # Act
    res = client.get("/api/history")
    data = res.json()

    # Assert
    assert data["history"][0]["total"] == 27.00
    assert data["history"][0]["symbol"] == "AMC"
    assert data["history"][0]["shares"] == 3
    assert data["history"][0]["price"] == 9.00
    assert data["history"][0]["name"] == "AMC Entertainment Holdings, Inc."
    assert data["history"][0]["id"] == "64482332fae37cb0433e046c"
    assert data["history"][0]["user_id"] == "fakeuser"
    assert (
        data["history"][0]["date"]
        == datetime.strptime(
            "04/26/2023, 12:13PM", "%m/%d/%Y, %I:%M%p"
        ).isoformat()
    )

    # Cleanup
    app.dependency_overrides = {}
